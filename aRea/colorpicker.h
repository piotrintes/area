#ifndef COLORPICKER_H
#define COLORPICKER_H

#include <QWidget>

class ColorPicker : public QWidget
{
    Q_OBJECT
private:
    QColor current;
    QColor second;

    void swap();

public:
    explicit ColorPicker(QWidget *parent = nullptr);

protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);

signals:
    void colorChanged(QColor color);

};

#endif // COLORPICKER_H
