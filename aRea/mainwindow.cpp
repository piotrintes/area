#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    qDebug() << QApplication::applicationDirPath() + "/icon.png";

    setWindowIcon(QPixmap(QApplication::applicationDirPath() + "/icon.png"));

#ifdef Q_OS_WIN
    ui->buttonToolEraser->setIcon(QPixmap(QApplication::applicationDirPath() + "/icons/draw-eraser.png"));
    ui->buttonErase->setIcon(QPixmap(QApplication::applicationDirPath() + "/icons/edit-delete.png"));
    ui->buttonOpen->setIcon(QPixmap(QApplication::applicationDirPath() + "/icons/document-open.png"));
    ui->buttonSaveAs->setIcon(QPixmap(QApplication::applicationDirPath() + "/icons/document-save-as.png"));
    ui->buttonUndo->setIcon(QPixmap(QApplication::applicationDirPath() + "/icons/edit-undo.png"));
#endif

    connect(ui->colorPicker,&ColorPicker::colorChanged,ui->canvas,&Canvas::setColor);
    connect(ui->scrollArea,&ScrollArea::moved,ui->canvas,&Canvas::move);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_buttonUndo_clicked()
{
    ui->canvas->undo();
}


void MainWindow::on_buttonToolEraser_clicked(bool checked)
{
    ui->canvas->setEraserActive(checked);
}


void MainWindow::on_buttonErase_clicked()
{
    ui->canvas->reset();
}

void MainWindow::on_buttonSaveAs_clicked()
{
    QFileInfo file(QFileDialog::getSaveFileName(this, "Save as", "", "Image file (*.jpg)"));
    if (file.path() == "") return;
    ui->canvas->saveToFile(file.absoluteFilePath());
}


void MainWindow::on_buttonOpen_clicked()
{
    QFileInfo file(QFileDialog::getOpenFileName(this, "Open", "", "Image file (*.jpg)"));
    if (file.path() == "") return;
    ui->canvas->loadFromFile(file.absoluteFilePath());

    if(QMessageBox::question(this,"Autosave","Do you want to edit this file directly?") == QMessageBox::Yes)
        ui->canvas->setAutosave(file.absoluteFilePath());
    else
        ui->canvas->setAutosave(QDir::homePath() + "/.aRea/autosave.jpg");
}

