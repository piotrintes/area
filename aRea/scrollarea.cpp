#include "scrollarea.h"

#include <QMouseEvent>
#include <QPainter>
#include <QDebug>

ScrollArea::ScrollArea(QWidget *parent) : QWidget(parent){
}

void ScrollArea::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    painter.setPen(QColor(0,0,0,0));

    painter.setBrush(palette().dark());
    painter.drawRoundedRect(16,0,width()-32,height(),12,12);
}

void ScrollArea::mouseMoveEvent(QMouseEvent *event)
{
    /*QTime current = QTime::currentTime();
    int elapsend = lastCheck.msecsTo(current);
    if(elapsend > 1000) speed = 1;
    lastCheck = current;*/

    int mov = start - event->pos().y();

    if(mov > 50 || mov < -50) {
        /*int d = mov;
        if(d < 0) d = -d;
        speed += (d/10 * elapsend)/10;
        if (speed > 10) speed = 10;
        mov *= speed;*/
        mov *= 3;
    }

    emit moved(mov);

    start = event->pos().y();
}

void ScrollArea::mousePressEvent(QMouseEvent *event)
{
    start = event->pos().y();
}
