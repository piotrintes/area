#ifndef SCROLLAREA_H
#define SCROLLAREA_H

#include <QTime>
#include <QWidget>

class ScrollArea : public QWidget
{
    Q_OBJECT
private:
    int start;
    //int speed = 1;
    //QTime lastCheck = QTime::currentTime();

public:
    explicit ScrollArea(QWidget *parent = nullptr);

protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);

signals:
    void moved(int movement);

};

#endif // SCROLLAREA_H
