#include "canvas.h"

#include <QMouseEvent>
#include <QPainter>
#include <QDebug>

Canvas::Canvas(QWidget *parent) : QWidget(parent)
{
    QDir dir(QDir::homePath() + "/.aRea/");
    if(dir.exists()) {
        picture = new QPixmap();
        loadFromFile(autosave);
    } else {
        picture = new QPixmap(width(),height());
        picture->fill();
    }

    pen = QPen(QColor(0,0,0),10,Qt::PenStyle::SolidLine,Qt::PenCapStyle::RoundCap);
    eraser = QPen(QColor(255,255,255),40,Qt::PenStyle::SolidLine,Qt::PenCapStyle::RoundCap);
}

void Canvas::undo()
{
    if(prev.isEmpty()) return;

    QPixmap *newPx = new QPixmap(picture->width(),picture->height());
    newPx->fill();
    QPainter painter(newPx);
    painter.drawPixmap(0,0,prev.last().width(),prev.last().height(),prev.last());
    painter.end();
    delete picture;
    picture = newPx;
    prev.removeLast();
    repaint();
}

void Canvas::reset()
{
    offset = 0;
    prev.clear();
    delete picture;
    picture = new QPixmap(width(),height());
    picture->fill();
    repaint();
}

void Canvas::extend(int newWidth, int newHeight)
{
    if(picture->width() > newWidth) newWidth = picture->width();
    if(picture->height() > newHeight) newHeight = picture->height();

    if(newWidth > picture->width() || newHeight > picture->height()) {
        //extend pixmap
        QPixmap *newPx = new QPixmap(newWidth,newHeight);
        newPx->fill();
        QPainter painter(newPx);
        painter.drawPixmap(0,0,picture->width(),picture->height(),*picture);
        painter.end();
        delete picture;
        picture = newPx;
    }
}

void Canvas::move(int movement)
{
    offset += movement;
    if(offset < 0) offset = 0;
    repaint();
}

void Canvas::setColor(QColor color) {
    pen.setColor(color);
}

void Canvas::setEraserActive(bool newEraserActive)
{
    eraserActive = newEraserActive;
}

void Canvas::loadFromFile(QString filename)
{
    offset = 0;
    prev.clear();
    delete picture;
    picture = new QPixmap();
    picture->load(filename);
}

void Canvas::saveToFile(QString filename)
{
    picture->save(filename);
}

void Canvas::setAutosave(QString filename)
{
    autosave = filename;
}

void Canvas::mouseMoveEvent(QMouseEvent *event)
{
    QTime current =  QTime::currentTime();
    QPoint point(event->pos().x(), event->pos().y() + offset);

    if(point.y() + 100 > picture->height()) extend(width(), point.y() + 100);

    QPainter painter(picture);
    painter.setRenderHint(QPainter::Antialiasing, true);
    if(eraserActive) painter.setPen(eraser); else painter.setPen(pen);

    if(lastTouchTime.msecsTo(current) > 100)
        painter.drawPoint(point);
    else
        painter.drawLine(lastTouchPoint,point);

    lastTouchTime = QTime::currentTime();
    lastTouchPoint = point;
    repaint();
}

void Canvas::mousePressEvent(QMouseEvent *event)
{
    prev.append(*picture);
    if(prev.size() > 20) prev.removeFirst();
}

void Canvas::mouseReleaseEvent(QMouseEvent *event)
{
    QDir dir(QDir::homePath() + "/.aRea/");
    if(!dir.exists()) dir.mkdir(QDir::homePath() + "/.aRea/");

    saveToFile(autosave);
}

void Canvas::resizeEvent(QResizeEvent *event)
{
    extend(width(),height());
}

void Canvas::wheelEvent(QWheelEvent *event)
{
    move(-event->angleDelta().y());
}

void Canvas::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    painter.setBrush(QColor(255,255,255));
    painter.setPen(QColor(0,0,0,0));
    painter.drawRect(0,0,width(),height());
    painter.drawPixmap(0,-offset,picture->width(),picture->height(),*picture);
}
