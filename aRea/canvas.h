#ifndef CANVAS_H
#define CANVAS_H

#include <QDir>
#include <QPainter>
#include <QTime>
#include <QWidget>

class Canvas : public QWidget
{
    Q_OBJECT
private:
    QPixmap *picture;
    QList<QPixmap> prev;
    QPen pen;
    QPen eraser;
    QTime lastTouchTime = QTime::currentTime();
    QPoint lastTouchPoint;

    int offset = 0;
    bool eraserActive = false;

    QString autosave = QDir::homePath() + "/.aRea/autosave.jpg";

    void extend(int newWidth, int newHeight);
public:
    explicit Canvas(QWidget *parent = nullptr);

    void undo();
    void reset();
    void move(int movement);
    void setEraserActive(bool newEraserActive);

    void loadFromFile(QString filename);
    void saveToFile(QString filename);
    void setAutosave(QString filename);

public slots:
    void setColor(QColor color);

protected:
    virtual void paintEvent(QPaintEvent *event);

    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);

    virtual void resizeEvent(QResizeEvent *event);
    virtual void wheelEvent(QWheelEvent *event);

signals:


};

#endif // CANVAS_H
