#include "colorpicker.h"

#include <QMouseEvent>
#include <QPainter>
#include <QColorDialog>

ColorPicker::ColorPicker(QWidget *parent) : QWidget(parent) {
    current = QColor(0,0,0);
    second = QColor(255,0,0);
}

void ColorPicker::swap() {
    QColor c = current;
    current = second;
    second = c;
    emit colorChanged(current);
    repaint();
}

void ColorPicker::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    painter.setPen(QPen(QColor(0,0,0),1));

#ifdef Q_OS_WIN
    painter.setBrush(second);
    painter.drawRoundedRect(38,32,40,40,6,6);
    painter.setBrush(current);
    painter.drawRoundedRect(06,16,48,48,6,6);
#else
    painter.setBrush(second);
    painter.drawRoundedRect(48,32,40,40,6,6);
    painter.setBrush(current);
    painter.drawRoundedRect(16,16,48,48,6,6);
#endif
}

void ColorPicker::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button() == 1){
        swap();
    } else {
        QColor c = QColorDialog::getColor(current);
        if (c.isValid()) {
            current = c;
            emit colorChanged(current);
            repaint();
        }
    }
}
